import argparse
import json
from typing import List

import requests

API_GET_COMPLETED_CHALLENGES = "https://exercism.org/api/v2/profiles/{username}/solutions?"
API_GET_TRACK_CHALLENGES = "https://exercism.org/api/v2/tracks/{language}/exercises"

CHALLENGES_FILE = 'challenges.json'


def get_all_pages(url) -> list:
    """Request all pages and return concatianted result

    note: expects the url to have `?`
    """
    req_json = requests.get(url).json()

    meta = req_json.get("meta", {})
    if meta == {}:
        raise (ValueError(req_json.get("reason", "Unknown error")))

    total_pages = meta.get("total_pages", 1)

    total_results = req_json.get("results", [])

    for page_num in range(2, total_pages + 1):

        req_json = requests.get(f"{url}&page={page_num}").json()

        results = req_json.get("results", None)
        if results is None:
            raise (ValueError(req_json.get("reason", "Unknown error")))

        total_results += results

    return total_results


def get_all_solved_challenges(username: str, language: str) -> list:
    return list(
        filter(
            lambda challenge: language.lower() == challenge.get("language", ''),
            get_all_pages(
                API_GET_COMPLETED_CHALLENGES.format(username=username)),
        )
    )


def proceses_challenges(challenges: List[dict], language: str) -> List[dict]:
    with open(CHALLENGES_FILE, 'r') as fp:
        local_challenges = json.load(fp)
    lang_challenges = local_challenges.get(language, {})

    challenges_names = [c.get('exercise', {}).get('title') for c in challenges]

    return [c for c in lang_challenges if c.get('name') in challenges_names]


def calc_points_for_lang(username: str, lang: str) -> float:
    solved = get_all_solved_challenges(username, lang)
    challenges = proceses_challenges(solved, lang)
    return sum(c.get('points') for c in challenges)


def get_track_challenges(language: str) -> List[dict]:

    req_json = requests.get(
        API_GET_TRACK_CHALLENGES.format(language=language)).json()

    json_exercises = req_json.get('exercises', [])
    exercises = []
    for exe in json_exercises:
        exercises.append({
            'name': exe.get('title'),
            'slug': exe.get('slug'),
            'points': 0,
            'why': f"AUTO ADDED: {exe.get('type', 'unknown type')} - {exe.get('difficulty', 'unknown difficulty')} - {exe.get('blurb', 'unknown description')}",
        })
    return exercises


def calc(username: str, languages: List[str]):
    for lang in languages:
        points = calc_points_for_lang(username, lang)
        print(lang, "-", points)


def pull(language: str, challenges: List[str]):

    with open(CHALLENGES_FILE, 'r') as fp:
        local_challenges = json.load(fp)

    language_challenges = local_challenges.get(language, [])
    language_challenges_names = [c.get('slug') for c in language_challenges]
    new_challenges = get_track_challenges(language)

    new_challenges = [c for c in new_challenges if c.get(
        'slug') not in language_challenges_names]

    if challenges:
        new_challenges = [
            c for c in new_challenges if c.get('slug') in challenges]

    language_challenges += new_challenges
    local_challenges[language] = language_challenges

    with open(CHALLENGES_FILE, 'w') as fp:
        json.dump(local_challenges, fp, indent=4)


def main():

    actions = {
        'calc': calc,
        'pull': pull,
    }

    parser = argparse.ArgumentParser(
        description="Calculate score for Exercism on beta.")
    subparsers = parser.add_subparsers(help='sub-command help', dest='action')

    parser_calc = subparsers.add_parser('calc', help='calc help')
    parser_calc.add_argument("username", help="Username on Exercism")
    parser_calc.add_argument(
        "--languages",
        "-l",
        metavar="LANG",
        type=str,
        nargs="+",
        help="Languages to calculate score on",
        required=True,
    )

    parser_pull = subparsers.add_parser('pull', help='pull help')
    parser_pull.add_argument(
        "language",
        type=str,
        help="Language to pull challenges",
    )

    parser_pull.add_argument(
        "--challenges",
        metavar="CHALLENGE",
        type=str,
        nargs="+",
        help="Challenges to pull. if not used, pulling all language challenges",
    )

    args = parser.parse_args()

    if args.action is None:
        parser.print_help()
        exit(1)

    func_args = vars(args)

    action = func_args['action']

    del func_args['action']

    actions[action](**func_args)


if __name__ == "__main__":
    main()
