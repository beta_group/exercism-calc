from datetime import datetime, timedelta
from functools import lru_cache, wraps

from flask import Flask

from exercism_calc import calc_points_for_lang

app = Flask(__name__)


def timed_lru_cache(minutes: int, maxsize: int = 128):

    def wrapper_cache(func):
        func = lru_cache(maxsize=maxsize)(func)
        func.lifetime = timedelta(minutes=minutes)
        func.expiration = datetime.utcnow() + func.lifetime

        @wraps(func)
        def wrapped_func(*args, **kwargs):
            if datetime.utcnow() >= func.expiration:
                func.cache_clear()
                func.expiration = datetime.utcnow() + func.lifetime

            return func(*args, **kwargs)

        return wrapped_func

    return wrapper_cache


@timed_lru_cache(minutes=15)
def calc_points_for_lang_cached(username: str, language: str) -> float:
    return calc_points_for_lang(username, language)


@app.route('/score/<username>/<language>')
def score(username: str, language: str):
    score = calc_points_for_lang_cached(username, language)
    return {
        'username': username,
        'language': language,
        'score': score,
        'rounded_score': round(score),
    }


@app.route("/")
def index():
    return """routes:<br>
    /score/&lt;username&gt;/&lt;language&gt; - to get points of user<br>
    """
